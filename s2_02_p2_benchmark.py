"""
SAE S2.02, part2, code benchmark

This script intends to benchmark function implementations done in `s2_02_p2_list.py`,`s2_02_p2_matrix.py` and  `s2_02_p2_dictionary.py`
(scripts supposed to be in the same directory).

In order to benchmark either one script or another, simply uncomment the relevant import.
"""
# uncomment only this import in order to validate matrix version
#from s2_02_p2_matrix import *

# uncomment only this import in order to validate list version
from s2_02_p2_list import *

# uncomment only this import in order to validate dictionary version
#from s2_02_p2_dictionary import *

import random
import time
import math


def load_data(data_dir):
    """
    load data from txt/csv files (supposed to be in ./datasets/dir_name directory).
    :param data_dir: (str) data directory
    :return: (None)
    """

    load_users("./datasets/" + data_dir)
    load_following_relationships("./datasets/" + data_dir)


def benchmark_get_minimal_path_length_from_user_to_other():
    """
    benchmark get_minimal_path_length_from_user_to_other function call with random parameter values.
    :return: (float) execution time (in multiple of 0.1 ms)
    """

    user1 = random.choice(users)  # To be completed: should be randomly chosen among users
    user2 = random.choice(users)  # To be completed: should be randomly chosen among users

    ###################
    start = time.perf_counter()
    ###################
    # (start measuring time)

    get_minimal_path_length_from_user_to_other(user1, user2)

    ###################
    end = time.perf_counter()
    ###################
    # (stop measuring time and return duration)

    return ((end - start)*10000)


def benchmark_get_users_linked_to():
    """
    benchmark get_minimal_path_length_from_user_to_other function call with random parameter values.
    :return: (float) execution time (in multiple of 0.1 ms)
    """

    user = random.choice(users)  # To be completed: should be randomly chosen among users

    ###################
    start2 = time.perf_counter()
    ###################
    # (start measuring time)

    get_users_linked_to(user)

    ###################
    end2 = time.perf_counter()
    ###################
    # (stop measuring time and return duration)
    return ((end2 - start2)*10000)

def benchmark_get_followers_of():
    """
    benchmark get_followers_of function call with random parameter values.
    :return: (float) execution time (in multiple of 0.1 ms)
    """

    user = random.choice(users)  # To be completed: should be randomly chosen among users

    ###################
    start3 = time.perf_counter()
    ###################
    # (start measuring time)

    get_followers_of(user)

    ###################
    end3 = time.perf_counter()
    ###################
    # (stop measuring time and return duration)
    return ((end3 - start3)*10000)


def benchmark_function(funct, count):
    """
    benchmark function on a given dataset (given count of function calls).
    :param funct: (fun) function to benchmark
    :param count: (int) number of function calls to perform
    :return: (float, float, float) min, max and average execution time (in ms)
    """

    durations = []

    for call in range(count):
        durations.append(funct())

    ###################
    minimum = min(durations)
    maximum = max(durations)
    avg = sum(durations)/count
    ###################
    return (minimum/10),(maximum/10),(avg/10)



def benchmark_data_loading(data_dir):
    """
    benchmark data loading on a given dataset.
    :param data_dir: (str) dataset directory
    :return: (float) data loading time (in ms)
    """

    ###################
    start4 = time.perf_counter()
    ###################
    # (start measuring time)

    load_data(data_dir)

    ###################
    end4 = time.perf_counter()
    ###################
    # (stop measuring time and return duration)

    return ((end4 - start4)*1000)


"""
benchmark a set of functions, for given datasets.
"""

dataset_ids = ["100", "250", "500", "1000", "2500"]
benchmark_specs = [(benchmark_get_followers_of, 200, dataset_ids[0:4]),
                   (benchmark_get_users_linked_to, 200, dataset_ids[0:4])]

for benchmark_spec in benchmark_specs:
    function, calls, dataset_dirs = benchmark_spec
    for dataset_dir in dataset_dirs:
        print("Nb users: " + dataset_dir, end=" ; ")
        print(" Chargement : " + str(benchmark_data_loading(dataset_dir)), end=" ; ")
        b_min, b_max, b_avg = benchmark_function(function, calls)
        print(" Fonction: " + str(function.__name__), end=" ; ")
        print("min: " + str(b_min) + " ; " + "max: " + str(b_max) + " ; " + "moy: " + str(b_avg))
    print("--------------------------------------------------------------------------------------------------------")
