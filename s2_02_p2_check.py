"""
SAE S2.02, part2, code validator

This script intends to validate function implementations done in `s2_02_p2_list.py`,`s2_02_p2_matrix.py` and  `s2_02_p2_dictionary.py`
(scripts supposed to be in the same directory).

In order to validate either one script or another, simply uncomment the relevant import.
"""
# uncomment only this import in order to validate matrix version
from s2_02_p2_matrix import *

# uncomment only this import in order to validate list version
#from s2_02_p2_list import *

# uncomment only this import in order to validate dictionary version
#from s2_02_p2_dictionary import *

""" 
expected results for question 1
"""
e1 = {'Katie White':
          [],
      'Jennifer Perez':
          ['John Stein', 'Katie White', 'Kim Taylor', 'Robert Holland'],
      'Troy Harrell':
          ['John Stein', 'Kim Taylor', 'Michael Fisher', 'Robert Holland', 'Troy Harrell'],
      'Robert Holland':
          ['John Stein', 'Kim Taylor', 'Robert Holland'],
      'John Stein':
          ['John Stein', 'Kim Taylor', 'Robert Holland'],
      'Michael Fisher':
          ['John Stein', 'Kim Taylor', 'Michael Fisher', 'Robert Holland', 'Troy Harrell'],
      'Caleb Perez':
          ['John Stein', 'Kim Taylor', 'Robert Holland'],
      'Kim Taylor':
          [],
      }

""" 
expected results for question 2
"""
e2 = {
        ('Katie White', 'Jennifer Perez'): True,
        ('Jennifer Perez', 'Katie White'): False,
        ('Katie White', 'Katie White'): False,
        ('Kim Taylor', 'John Stein'): True,
        ('Kim Taylor', 'Jennifer Perez'): True,
        ('Kim Taylor', 'Katie White'): False,
        ('John Stein', 'Caleb Perez'): True,
        ('Robert Holland', 'Robert Holland'): True,
        ('Robert Holland', 'Caleb Perez'): True,
        ('Robert Holland', 'Troy Harrell'): True
      }

""" 
expected results for question 3
"""
e3 = {
        ('Katie White', 'Jennifer Perez'): 1,
        ('Jennifer Perez', 'Katie White'): -1,
        ('Katie White', 'Katie White'): 0,
        ('Kim Taylor', 'John Stein'): 1,
        ('Kim Taylor', 'Jennifer Perez'): 2,
        ('Kim Taylor', 'Katie White'): -1,
        ('John Stein', 'Caleb Perez'): 1,
        ('Robert Holland', 'Robert Holland'): 0,
        ('Robert Holland', 'Caleb Perez'): 2,
        ('Robert Holland', 'Troy Harrell'): 3
      }


def load_data():
    """
    load data from txt/csv files (supposed to be in the same directory)
    :return: (None)
    """

    load_users("./datasets/check/")

    load_following_relationships("./datasets/check/")


def validate():
    """
    validate list/matrix function implementations, printing PASSED/FAILED for each test case
    as well as expected vs returned if FAILED.

    :return: (None)
    """

    load_data()

    print("QUESTION 1")
    inputs = [
                'Katie White', 'Jennifer Perez', 'Troy Harrell', 'Robert Holland',
                'John Stein', 'Michael Fisher', 'Caleb Perez', 'Kim Taylor'
            ]

    validate_function(get_users_linked_to, e1, inputs)

    print()
    print("QUESTION 2")
    inputs = [
        ('Katie White', 'Jennifer Perez'),
        ('Jennifer Perez', 'Katie White'),
        ('Katie White', 'Katie White'),
        ('Kim Taylor', 'John Stein'),
        ('Kim Taylor', 'Jennifer Perez'),
        ('Kim Taylor', 'Katie White'),
        ('John Stein', 'Caleb Perez'),
        ('Robert Holland', 'Robert Holland'),
        ('Robert Holland', 'Caleb Perez'),
        ('Robert Holland', 'Troy Harrell')
    ]
    validate_function(is_user_linked_to_other, e2, inputs, tuple_input=True)

    print()
    print("QUESTION 3")
    validate_function(get_minimal_path_length_from_user_to_other, e3, inputs, tuple_input=True)


def validate_function(function, expected, input_list=None, tuple_input=False):
    """
    validate a specific function
    :param function: function to check
    :param expected: expected result
    :param input_list: list containing inputs (test cases)
    :return: (None)
    """

    returned = {}
    if input_list is None:
        returned = function()
        if returned == expected:
            print("(PASSED)")
        else:
            print("(FAILED)")
            print("expected: ", expected)
            print("returned: ", returned)
    else:
        for element in input_list:
            if tuple_input:
                returned[element] = function(*element)
            else:
                returned[element] = function(element)
            result = "(PASSED)" if returned[element] == expected[element] else "(FAILED)"
            print(result, end='\t')
            print(element)
            if result == "(FAILED)":
                print("expected: ", expected[element])
                print("returned: ", returned[element])


validate()
