"""
SAE S2.02, part2, common functions/variables
"""

"""
users (list of strings)
"""
users = []


def load_users(dir_name):
    """
    load users from `users.txt` file (supposed to be in dir_name directory)
    :return: (None)
    """

    users.clear()
    data_file = open(dir_name+"/users.txt", 'r')

    for line in data_file:
        users.append(line.strip())

    data_file.close()