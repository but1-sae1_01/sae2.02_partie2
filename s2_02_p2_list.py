"""
SAE S2.02, part2, implementation using successor lists
"""


"""
Graph problem, part1, implementation using LS and TS lists
"""
from s2_02_p2_common import *

"""
following relationships 
lists where user indexes are same as in `users` 
"""
FOLLOWING_RELATIONSHIPS_LS = []
FOLLOWING_RELATIONSHIPS_TS = []



def load_following_relationships(dir_name) -> None:
    """
    load following relationships from `following_relationships.csv` file (supposed to be in the same directory)
    :return: (None)
    """
    global FOLLOWING_RELATIONSHIPS_TS  # to modify the content of the global variable

    number_of_users = len(users)
    FOLLOWING_RELATIONSHIPS_TS = [0] * number_of_users
    data_file = open(dir_name+"/following_relationships.csv", 'r')

    for line in data_file:
        line = line.strip()

        if line:
            user, friend = line.split(",")
            user_id = users.index(friend)
            FOLLOWING_RELATIONSHIPS_LS.insert(FOLLOWING_RELATIONSHIPS_TS[user_id], user)

            for ts_index in range(user_id, number_of_users):
                FOLLOWING_RELATIONSHIPS_TS[ts_index] += 1

    FOLLOWING_RELATIONSHIPS_TS.insert(0, 0)
    data_file.close()


def get_followers_of(followee: str) -> list[str]:
    """
    get followers of a specific user
    :param followee: (str) a user
    :return: (list[str]) a sorted list, containing all followers of `followee` (without duplicate)
    """
    followers = list()

    if followee in users: # -> O(n)
        followee_id = users.index(followee) # -> O(n)
        start = FOLLOWING_RELATIONSHIPS_TS[followee_id]
        end = FOLLOWING_RELATIONSHIPS_TS[followee_id + 1]
        followers = FOLLOWING_RELATIONSHIPS_LS[start:end]

    return sorted(followers) #-> O(n*log(n))
    #Complexité finale: O(n²+nlog(n))


def get_users_linked_to(user):
    """
    get users linked to a given user.
    direct followers of user are considered to be linked to it,
    followers of followers of a user are also considered to be linked to it, ...

    :param user: (str) a user
    :return: (list[str]) a sorted list, containing all users linked to user.
             N.B. A same user must only be present once in the result.
    """
    linked_users = set()
    marques=[]
    newMarques=[user]
    while newMarques:
        L=[]
        for point in newMarques: #pire des cas -> O(n)
            for voisin in get_followers_of(point): # parcours n fois * complexité de get_followers_of()
                                                   # -> O(n)*O(n)*(n²+nlog(n))
                if voisin not in marques: #pire des cas -> O(n)*O(n)*O(n)*(n²+nlog(n))
                    marques.append(voisin)
                    L.append(voisin)
                    linked_users.add(voisin)
        newMarques=L
    
    return sorted(list(linked_users)) #-> O(n log(n))
    #Complexité finale: O(n log(n) + On³*(n²+n log(n)) = O(n log(n) + n⁵)


def is_user_linked_to_other(user, other):
    """
    check if a given user is linked to another.

    :param user: (str) a user
    :param other: (str) a user
    :return: (bool) true if user is linked to other, false else.
    """
    if user in get_users_linked_to(other): #complexite de get_users_linked_to() -> O(n log(n) + n⁵)* O(n)
        return True
    return False
    #Complexité finale: O(n log(n) + n⁵)* O(n) = O(n⁶)


def get_minimal_path_length_from_user_to_other(user, other):
    """
    get minimal path length from a user supposed to be linked to another.

    where there is no link, path length is -1.
    where both users are the same, path length is 0
    where there is a direct link, path length is 1.
    where there is a link via another user, path length is 2, ...

    :param user: (str) a user
    :param other: (str) a user
    :return: (int) path lengor voisin_index in oldMarques:
            for voisin in get_folth
    """
    if user == other:
        return 0

    if is_user_linked_to_other(user,other) == False: #complexite de get_users_linked_to() -> O(n log(n) + n⁵)* O(n)
        return -1

    D = 0
    marques=[other]
    oldMarques=[other]
    
    while user not in marques:
        L = []
        for voisin_index in oldMarques: # pire des cas -> O(n)
            for voisin in get_followers_of(voisin_index): # parcours n fois + complexité de get_followers_of()
                                                          # -> O(n²+nlog(n))*O(n) = n³+n²log(n)
                if voisin not in marques: # pire des cas -> O(n)
                    marques.append(voisin)
                    L.append(voisin)
        D += 1
        oldMarques = L
    return D 
    #Complexité finale: O(n²log(n)+n⁶)+n³+n³log(n)

