"""
SAE S2.02, part2, implementation using successor matrices
"""
from s2_02_p2_common import *
import time

"""
following relationships 
matrix where user indexes are same as in `users` 
"""
following_relationships = []


def load_following_relationships(dir_name):
    """
    load following relationships from `following_relationships.csv` file (supposed to be in the same directory)
    :return: (None)
    """

    global following_relationships

    data_file = open(dir_name + "/following_relationships.csv", 'r')
    number_of_users = len(users)

    following_relationships = [[0] * number_of_users for k in range(number_of_users)]

    for line in data_file:

        line = line.strip()

        if line:
            user, friend = line.split(",")

            following_relationships[users.index(user)][users.index(friend)] = 1

    data_file.close()


def transpose(matrix):
    """
    transpose matrix
    :param matrix: (list[list[int]]) matrix
    :return: (list[list[int]]) transposed matrix
    """

    nb_of_lines = len(matrix)

    nb_of_columns = len(matrix[0])

    transposed_matrix = [[None] * nb_of_lines for k in range(nb_of_columns)]

    for line_index in range(nb_of_lines): #O(n) 

        for column_index in range(nb_of_columns): #O(n)* O(n) = O(n²)
            transposed_matrix[column_index][line_index] = matrix[line_index][column_index]

    return transposed_matrix


def boolean_addition(list1, list2):
    """
    boolean matrix addition (matrices dimensions are supposed to be valid)
    :param list1: (list[list[int]]) first matrix
    :param list2: (list[list[int]]) second matrix
    :return: (list[int]) matrix1 + matrix2
    """
    nb_of_lines = len(list1)
    nb_of_columns = len(list1[0])

    addition = [[None] * nb_of_columns for k in range(nb_of_lines)]

    for index_line in range(nb_of_lines): #O(n)
        for index_column in range(nb_of_columns): #O(n)*O(n) = O(n²)
            if ((list1[index_line][index_column] == 1) or (list2[index_line][index_column]==1)):
                addition[index_line][index_column] = 1
            else:
                addition[index_line][index_column] = 0
    return addition


def boolean_product(matrix1, matrix2):
    """
    boalean matrix product
    :param matrix1: (list[list[int]]) first matrix
    :param matrix2: (list[list[int]]) second matrix
    :return: (list[list[int]]) matrix1 * matrix2, or [] if dimensions are invalid
    """
    if len(matrix1[0]) != len(matrix2):
        return []

    common_dimension = len(matrix2)

    nb_of_lines = len(matrix1)
    nb_of_columns = len(matrix2[0])

    result = [[None] * nb_of_columns for k in range(nb_of_lines)]

    for index_line in range(nb_of_lines): #O(n)
        for index_column in range(nb_of_columns): #O(n)*O(n)
            result[index_line][index_column] = 1 * bool(
                sum([matrix1[index_line][k] and matrix2[k][index_column] for k in range(common_dimension)]))
    return result


def get_followers_of(followee):
    """
    get followers of a specific user
    :param followee: (str) a user
    :return: (list[str]) a sorted list, containing all followers of `followee` (without duplicate)
    """
    followers = []

    nb_of_users = len(users)

    followee_index = users.index(followee) #O(n)

    for follower_index in range(nb_of_users): # -> O(n) + O(n)

        if following_relationships[follower_index][followee_index]:
            followers.append(users[follower_index])

    return sorted(followers) #O(n log(n)) + O(n) + O(n)

def get_users_linked_to(user):
    """
    get users linked to a given user.
    direct followers of user are considered to be linked to it,
    followers of followers of a user are also considered to be linked to it, ...

    :param user: (str) a user
    :return: (list[str]) a sorted list, containing all users linked to user.
             N.B. A same user must only be present once in the result.
    """
    linked_users = set()
    marques=[]
    newMarques=[user]
    while newMarques:
        L=[]
        for point in newMarques: # pire des cas -> O(n)
            for voisin in get_followers_of(point): # parcours n fois + complexité de get_followers_of()
                                                   # -> O(n log(n)) + O(n) + O(n)* O(n) = O(n log(n)) + O(n) + O(n²)
                if voisin not in marques: # O(n log(n)) + O(n) + O(n²) * O(n) = O(n log(n)) + O(n) + O(n³)
                    marques.append(voisin)
                    L.append(voisin)
                    linked_users.add(voisin)
        newMarques=L
    
    return sorted(list(linked_users)) # (n log(n)) + O(n log(n)) + O(n) + O(n³)


def is_user_linked_to_other(user, other):
    """
    check if a given user is linked to another.

    :param user: (str) a user
    :param other: (str) a user
    :return: (bool) true if user is linked to other, false else.
    """
    if user in get_users_linked_to(other): # (n log(n)) + O(n log(n)) + O(n) + O(n³) * O(n) = (n log(n)) + O(n log(n)) + O(n) + O(n⁴)
        return True
    return False


def get_minimal_path_length_from_user_to_other(user, other):
    """
    get minimal path length from a user supposed to be linked to another.

    where there is no link, path length is -1.
    where both users are the same, path length is 0
    where there is a direct link, path length is 1.
    where there is a link via another user, path length is 2, ...

    :param user: (str) a user
    :param other: (str) a user
    :return: (int) path length
    """
    if user == other:
        return 0
    
    D = 0
    marques=[]
    newMarques=[user]
    
    while newMarques: # while + test liste vide -> pas de compléxité particulière
        D += 1
        L = []
        for point in newMarques: # pire des cas -> O(n)
            point_index = users.index(point) # pire des cas -> O(n)
            for voisin_index in range(len(following_relationships[point_index])): # pire des cas -> O(n)
                if following_relationships[point_index][voisin_index] == 1:
                    voisin = users[voisin_index]
                    if voisin == other:
                        return D
                    if voisin not in marques: #O(n)
                        marques.append(voisin)
                        L.append(voisin)
        newMarques = L
    
    return -1
# O(n)*O(n)*O(n)*O(n) = O(n⁴)