"""
SAE S2.02, part2, implementation using successor lists
"""
from s2_02_p2_common import *

"""
following relationships 
(dict where key is a user and value is a list of users)
"""
following_relationships = {}


def load_following_relationships(dir_name):
    """
    load following relationships from `following_relationships.csv` file (supposed to be in dir_name directory)
    :return: (None)
    """
    following_relationships.clear()
    data_file = open(dir_name+"/following_relationships.csv", 'r')

    for line in data_file:

        line = line.strip()

        if line:

            user, friend = line.split(",")

            if user not in following_relationships.keys():
                following_relationships[user] = []

            following_relationships[user].append(friend)

    data_file.close()


def get_followers_of(followee):
    """
    get followers of a specific user
    :param followee: (str) a user
    :return: (list[str]) a sorted list, containing all followers of `followee` (without duplicate)
    """
    followers = []

    for user, user_followees in following_relationships.items(): # in + bouble for -> O(n²)
        if followee in user_followees: # -> O(n)
            followers.append(user)
    return sorted(followers) # -> O(n*log(n))
    #Complexité finale: O(n^3+n*log(n))


def get_users_linked_to(user):
    """
    get users linked to a given user.
    direct followers of user are considered to be linked to it,
    followers of followers of a user are also considered to be linked to it, ...

    :param user: (str) a user
    :return: (list[str]) a sorted list, containing all users linked to user.
             N.B. A same user must only be present once in the result.
    """
    linked_users = set()
    marques=[]
    newMarques=[user]
    while newMarques: # while + test liste vide -> pas de compléxité particulière
        L=[]
        for point in newMarques: # pire des cas -> O(n)
            for voisin in get_followers_of(point): # parcours n fois * complexité de get_followers_of()
                                                   # -> O(n)*O(n^3+n*log(n)) = O(n⁴+n²*log(n)) ~ O(n⁵)
                if voisin not in marques: # pire des cas -> O(n)
                    marques.append(voisin)
                    L.append(voisin)
                    linked_users.add(voisin)
        newMarques=L
    
    return sorted(list(linked_users)) # -> O(n*log(n))
    #Complexité finale: O(n⁴+n²log(n))×O(n)+n*log(n)) ~ O(n⁵)+n*log(n)


def is_user_linked_to_other(user, other):
    """
    check if a given user is linked to another.

    :param user: (str) a user
    :param other: (str) a user
    :return: (bool) true if user is linked to other, false else.
    """
    if user in get_users_linked_to(other): #complexite de get_users_linked_to() -> O(n⁵)+n*log(n)*O(n) = O(n⁵+n²log(n))
        return True
    return False


def get_minimal_path_length_from_user_to_other(user, other):
    """
    get minimal path length from a user supposed to be linked to another.

    where there is no link, path length is -1.
    where both users are the same, path length is 0
    where there is a direct link, path length is 1.
    where there is a link via another user, path length is 2, ...

    :param user: (str) a user
    :param other: (str) a user
    :return: (int) path length
    """
    if user == other:
        return 0

    marques = set()
    newMarques = [user]
    D = 0

    while newMarques: # while + test liste vide -> pas de compléxité particulière
        D += 1
        L = []
        for point in newMarques: # pire des cas -> O(n)
            if point in following_relationships: # pire des cas -> O(n)
                for voisin in following_relationships[point]: # pire des cas -> O(n)
                    if voisin == other:
                        return D
                    if voisin not in marques: # pire des cas -> O(n)
                        marques.add(voisin)
                        L.append(voisin)
        newMarques = L

    return -1
#O(n)*O(n)*O(n)*O(n) = 0(n⁴)